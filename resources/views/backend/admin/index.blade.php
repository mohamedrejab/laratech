@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        @if (session('message'))
                            <div class="alert alert-success" role="alert">
                                {{ session('message') }}
                            </div>
                        @endif
                            @include('layouts.navbar')
                            <div class="row mb-2 mt-3">
                                <div class="col-12">
                                    <a href="{{ route('auth.questions.create') }}" class="btn btn-primary float-right">Ajouter question</a>
                                </div>
                            </div>
                        <div class="row p-3">
                            <table class="table">
                                <thead class="thead-dark">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Question</th>
                                    <th scope="col">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($all as $question)
                                    <tr>
                                        <th scope="row">{{ $question->id }}</th>
                                        <td>{{ $question->question }}</td>
                                        <td>
                                            <a href="{{ route('auth.questions.show', [$question->id]) }}" class="btn btn-sm btn-secondary">Afficher</a>
                                            <a href="{{ route('auth.questions.edit', [$question->id])}}" class="btn btn-sm btn-primary">Modifier</a>
                                            <form action="{{ route('auth.questions.destroy', [$question->id]) }}" method="POST">
                                                <input type="hidden" name="_token" id="csrf-token" value="{{ \Illuminate\Support\Facades\Session::token() }}" />
                                                <input type="hidden" name="_method" value="delete" />
                                                <button type="submit" onclick="return myFunction();" class="btn btn-sm btn-danger mt-2">Supprimer</button>
                                            </form>
                                            <a href="{{ route('auth.questions.addReponse', [$question->id])}}" class="btn btn-sm btn-info">Ajouter réponses</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
