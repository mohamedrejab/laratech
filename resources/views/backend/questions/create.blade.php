@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        @if (session('errors'))
                            <div class="alert alert-danger" role="alert">
                                {{ session('errors') }}
                            </div>
                        @endif
                        @include('layouts.navbar')
                                <form method="POST" @if(!@$show) action="{{ route('auth.questions.store') }}" @endif>
                                    @csrf
                                    <div class="form-group mt-5">
                                        <label for="exampleFormControlTextarea1">Question:</label>
                                        <textarea class="form-control" id="exampleFormControlTextarea1" name="question" rows="3" {{(@$show)?'disabled':''}}>{{@$model->question}}</textarea>
                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group row">
                                                <label for="staticEmail" class="col-sm-2 col-form-label">Score:</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" name="score" id="staticEmail" value="{{@$model->score}}" {{(@$show)?'disabled':''}}>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group row">
                                                <label for="staticEmail1" class="col-sm-2 col-form-label">Type:</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" aria-label="Résultat" name="type" {{(@$show)?'disabled':''}}>
                                                        <option value="reponse" {{(@$model->type == 'reponse')? 'selected': ''}}>Une seule réponse</option>
                                                        <option value="multi" {{(@$model->type == 'multi')? 'selected': ''}}>Multi réponse</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-10 offset-2">
                                            <input type="hidden" name="id" value="{{@$model->id}}">
                                            <a type="button" href="{{route('auth.admin.index')}}"
                                               class="btn btn-light">Retour</a>
                                            @if(!@$show)
                                                <button type="submit"
                                                        class="btn btn-primary waves-effect waves-light">{{(@$model)? 'Mise à jours' : 'Ajouter' }}</button>
                                            @endif
                                        </div>
                                    </div>
                                </form>
                        @if(@$model)
                            @if(count($model->reponses) > 0)
                                @php $compteur = 1 @endphp
                                    <table class="table">
                                        <thead class="thead-dark">
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Reponse</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($model->reponses as $reponse)
                                            <tr>
                                                <th scope="row">{{ $compteur }}</th>
                                                <td>{{ $reponse->reponse }}</td>
                                                @if($reponse->resultat == 1)
                                                    <td>{{ 'Vrai' }}</td>
                                                @else
                                                    <td>{{ 'Faux' }}</td>
                                                @endif
                                            </tr>
                                        @php $compteur++ @endphp
                                        @endforeach
                                        </tbody>
                                    </table>
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
