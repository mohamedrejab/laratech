@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        @if (session('errors'))
                            <div class="alert alert-danger" role="alert">
                                {{ session('errors') }}
                            </div>
                        @endif
                        @include('layouts.navbar')
                        <form method="POST" @if(!@$show) action="{{ route('auth.reponses.store') }}" @endif>
                            @csrf
                            <input type="hidden" name="id_question" value="{{ @$id }}">
                            <div class="row p-3 copy" id="copy">
                                <div class="col-2">
                                    <span>Reponse</span>
                                </div>
                                <div class="col5">
                                    <input type="text" class="form-control" name="reponse[]" value="{{@$model->reponse}}">
                                </div>
                                <div class="col-3">
                                    <select class="form-control" aria-label="Résultat" name="resultat[]">
                                        <option value="true">Vrai</option>
                                        <option value="false">Faux</option>
                                    </select>
                                </div>
                                <div class="col-2 pt-1">
                                    <button type="button" class="btn btn-sm btn-primary add" id="addLigne"><i class="fas fa-plus"></i></button>
                                    <button type="button" class="btn btn-sm btn-danger remove" id="remove"><i class="fas fa-trash"></i></button>
                                </div>
                            </div>
                            <div id="copied">

                            </div>
                            <div class="form-group row">
                                <div class="col-sm-10 offset-2">
                                    <input type="hidden" name="id" value="{{@$model->id}}">
                                    <a type="button" href="{{route('auth.admin.index')}}"
                                       class="btn btn-light">Retour</a>
                                    @if(!@$show)
                                        <button type="submit"
                                                class="btn btn-primary waves-effect waves-light">{{(@$model)? 'Mise à jours' : 'Ajouter' }}</button>
                                    @endif
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
