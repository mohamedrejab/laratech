@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        @if (session('message'))
                            <div class="alert alert-success" role="alert">
                                {{ session('message') }}
                            </div>
                        @endif
                        @include('layouts.navbar')
                        <div class="row p-3">
                            <table class="table">
                                <thead class="thead-dark">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Réponse</th>
                                    <th scope="col">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $compteur = 1 @endphp
                                @foreach($all as $reponse)
                                    <tr>
                                        <th scope="row">{{ $compteur }}</th>
                                        <td>{{ $reponse->reponse }}</td>
                                        <td>
                                            <a href="{{ route('auth.reponses.show', [$reponse->id]) }}" class="btn btn-sm btn-secondary">Afficher</a>
                                            <a href="{{ route('auth.reponses.edit', [$reponse->id])}}" class="btn btn-sm btn-primary">Modifier</a>
                                            <form action="{{ route('auth.reponses.destroy', [$reponse->id]) }}" method="POST">
                                                <input type="hidden" name="_token" id="csrf-token" value="{{ \Illuminate\Support\Facades\Session::token() }}" />
                                                <input type="hidden" name="_method" value="delete" />
                                                <button type="submit" onclick="return myFunction();" class="btn btn-sm btn-danger mt-2">Supprimer</button>
                                            </form>
                                        </td>
                                    </tr>
                                    @php $compteur++ @endphp
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
