<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Laratech</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="{{ route('auth.admin.index') }}" @if((Route::current()->getName() == 'auth.admin.index')) style="font-weight: bold; color: black" @endif>Liste des questions</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('auth.reponses.index') }}" @if((Route::current()->getName() == 'auth.reponses.index')) style="font-weight: bold; color: black" @endif>Liste des réponses</a>
            </li>
        </ul>
    </div>
</nav>
