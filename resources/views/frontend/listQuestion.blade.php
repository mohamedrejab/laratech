@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        @if (session('score'))
                            <div class="alert alert-success" role="alert">
                                votre score est: {{ session('score') }}
                            </div>
                        @endif
                        <form method="post" action="{{ route('auth.qcm.calcul') }}">
                            @csrf
                            @foreach($all as $question)
                                <div class="card mb-3" style="width: 100%;">
                                    <div class="card-body">
                                        <h5 class="card-title">{{ $question->question }}</h5>
                                        @if(count($question->reponses) > 0)
                                            @if($question->type == "multi")
                                                @foreach($question->reponses as $reponse)
                                                    <div class="input-group-text mb-2">
                                                        <input type="checkbox" name="reponses[]" value="{{ $reponse->id }}.{{ $question->id }}"> <span class="ml-2">{{ $reponse->reponse }}</span>
                                                    </div>
                                                @endforeach
                                            @else
                                                @foreach($question->reponses as $reponse)
                                                    <div class="input-group-text mb-2">
                                                        <input type="radio" name="reponses[]" value="{{ $reponse->id }}.{{ $question->id }}"> <span class="ml-2">{{ $reponse->reponse }}</span>
                                                    </div>
                                                @endforeach
                                            @endif
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                            <button type="submit" class="btn btn-primary float-right">Valider</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
