<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::group(['prefix' => 'auth','as'=>'auth.', 'middleware' => ['auth']], function () {
    //Route index admin
    Route::get('/admin/index', 'QuestionController@index')->name('admin.index');

    //Questions Route
    Route::resource('questions', 'QuestionController');

    //Reponses Routes
    Route::get('/questions/addReponse/{id}', 'ReponseController@addReponse')->name('questions.addReponse');
    Route::post('/reponses/updated', 'ReponseController@update')->name('reponses.updated');
    Route::resource('reponses', 'ReponseController');

    //Route QCM User
    Route::get('/qcm/index', 'FrontendController@index')->name('qcm.index');
    Route::post('/qcm/calcule', 'FrontendController@calcul')->name('qcm.calcul');
});
