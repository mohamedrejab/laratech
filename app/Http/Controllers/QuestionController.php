<?php

namespace App\Http\Controllers;

use App\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class QuestionController extends Controller
{
    public function index(){
        $all = Question::all();
        return view('backend.admin.index',[
            "all" => $all
        ]);
    }
    public function create(){
        return view('backend.questions.create');
    }

    public function store(Request $request){
        $request->validate([
            'question' => 'required|min:4'
        ]);

        try {
            $modif = true;
            $model = Question::find($request->id);
            if (!$model) {
                $modif = false;
                $model = new Question();
            }
            $model->question = $request->question;
            $model->score = $request->score;
            $model->type = $request->type;
            $model->save();

        } catch (\Exception $e) {
            return redirect()->back()->with('errors', 'Erreur');
        }

        if($modif)
            return redirect()->route('auth.admin.index')->with('message', 'updated');
        else
            return redirect()->route('auth.admin.index')->with('message', 'created');
    }

    public function show($id){
        $show=true;
        $model=Question::with('reponses')->where('id',$id)->first();
        return view('backend.questions.create', [
            'show' => $show,
            'model'=> $model
        ]);
    }

    public function edit($id)
    {
        $model=Question::find($id);
        return view('backend.questions.create', [
            'model'=> $model
        ]);
    }

    public function destroy($id)
    {
        $model = Question::find($id);
        DB::table('questions_reponses')->where('question_id',$id)->delete();
        $model->delete();

        return back()->with('message', 'deleted');
    }
}
