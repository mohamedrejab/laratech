<?php

namespace App\Http\Controllers;

use App\Reponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReponseController extends Controller
{
    public function addReponse($id){
        return view('backend.reponses.create',[
            'id' => $id
        ]);
    }

    public function index(){
        $all = Reponse::all();
        return view('backend.reponses.index',[
            'all' => $all
        ]);
    }

    public function store(Request $request){
        $this->validate($request, [
            'reponse.*' => 'required',
        ]);
        $taille = count($request->reponse);
        for($i = 0; $i< $taille; $i++){
            $reponse = new Reponse();
            $reponse->reponse = $request->reponse[$i];
            if($request->resultat[$i] == "true"){
                $reponse->resultat = true;
            }else{
                $reponse->resultat = false;
            }

            $reponse->save();

            if ($reponse){
                $reponse->questions()->sync($request->id_question);
            }

        }
        return redirect()->route('auth.admin.index')->with('message', 'created');
    }

    public function edit($id){
        $model=Reponse::find($id);
        return view('backend.reponses.edit', [
            'model'=> $model
        ]);
    }

    public function update(Request $request){
        $model= Reponse::find($request->id);
        $model->reponse = $request->reponse;
        if($request->resultat == "true"){
            $model->resultat = true;
        }else{
            $model->resultat = false;
        }
        $model->save();

        return redirect()->route('auth.reponses.index')->with('message', 'updated');
    }
    public function show($id){
        $show=true;
        $model=Reponse::find($id);
        return view('backend.reponses.edit', [
            'show' => $show,
            'model'=> $model
        ]);
    }

    public function destroy($id){
        $model = Reponse::find($id);
        DB::table('questions_reponses')->where('reponse_id',$id)->delete();
        $model->delete();

        return back()->with('message', 'deleted');
    }
}
