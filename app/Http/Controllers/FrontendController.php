<?php

namespace App\Http\Controllers;

use App\Question;
use App\Reponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FrontendController extends Controller
{
    public function index(){
        $all = Question::with('reponses')->orderBy('id', 'asc')->get();
        return view('frontend.listQuestion', [
            'all' => $all
        ]);
    }

    public function calcul(Request $request){
        $iteration = 1;
        $increment = 0;
        foreach ($request->reponses as $reponse){
            $score[0] = 0;
            $res = explode(".", $reponse);
            $reponses = DB::table('questions_reponses')->where('question_id', $res[1])->get();
            $id_reponses_exact = [];
            foreach ($reponses as $rep){
                array_push($id_reponses_exact, $rep->reponse_id);
            }
            $question = Question::find($res[1]);
            if($question->type=="reponse"){
                $resultat = Reponse::find($res[0]);
                if($resultat->resultat === 1){
                    $increment = $increment+1;
                    array_push($score, $question->score);
                }
            }else{
                $rep = Reponse::whereIn('id', $id_reponses_exact)->get();
                $count = 0;
                foreach ($rep as $element){
                    if($element->resultat == 1)
                        $count++;
                }
                $part = $question->score / $count;

                $resultat = Reponse::find($res[0]);
                if($resultat->resultat == 1){
                    $increment = $increment+1;
                    array_push($score, $part);
                }
            }
            $iteration++;
        }
        $total = array_sum($score);
        return back()->with('score', $total);
    }
}
