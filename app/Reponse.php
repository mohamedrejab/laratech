<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reponse extends Model
{
    public function questions()
    {
        return $this->belongsToMany('App\Question', 'questions_reponses');
    }
}
