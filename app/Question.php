<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    public function reponses()
    {
        return $this->belongsToMany('App\Reponse', 'questions_reponses');
    }
}
