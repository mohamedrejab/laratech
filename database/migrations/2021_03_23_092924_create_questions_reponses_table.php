<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionsReponsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions_reponses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('reponse_id');
            $table->unsignedBigInteger('question_id');
            $table->timestamps();

            $table->foreign('reponse_id', 'reponse_id_foreign')
                ->references('id')->on('reponses')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            $table->foreign('question_id', 'question_id_foreign')
                ->references('id')->on('questions')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions_reponses');
    }
}
